from PySide2 import QtCore, QtGui, QtWidgets, QtQml
from src.ui import UIApp


class App(QtWidgets.QApplication):
    def __init__(self, args):
        super(App, self).__init__(args)
        self.ui_app = UIApp()
