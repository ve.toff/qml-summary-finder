# *QML Summary Finder*

## Known problems
>qt.qpa.plugin: Could not find the Qt platform plugin "xcb" in ""
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.
>Aborted

### Solution
`export QT_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/qt5/plugins` **or**<br>
`export QT_PLUGIN_PATH=/lib/x86_64-linux-gnu/qt5/plugins` **or** another path to your qt5-plugins directory.

> `...` <br>
> `self._engine.rootObjects()[0]`<br>
> `IndexError: list index out of range` <br>
> or some errors related to missing QtQuick modules.

### Solution
`export QML2_IMPORT_PATH=$HOME/.local/lib/python3.9/site-packages/PySide2/Qt/qml/` **or**<br>
`export QML2_IMPORT_PATH=/usr/local/lib/python3.9/dist-packages/PySide2`  **or** another path to python PySide2 library.
